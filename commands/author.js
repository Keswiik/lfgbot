exports.run = author
exports.desc = "Get the author of the invoking message"
exports.command = []

var utils = require('../utils.js')
var logger = require('../Logger.js')


async function author(bot, config, message) {
    logger.log('info', `${message.author}`)
    utils.sendMessage(message.channel, `${message.author.username}#${message.author.discriminator}`)
}
