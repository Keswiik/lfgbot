exports.run = addgame
exports.desc = "Adds a game to a guild's list of lfg games"
exports.command = ["addgame <game>"]

var utils = require('../utils.js')
var logger = require('../logger.js')

/**
* Adds a game to the server's game list in config.json
* @param {Discord.Client} bot - The bot client instance
* @param {Object} config - JSON representation of the bot's current configuration
* @param {Discord.Message} message - The message that invoked this command
*/
async function addgame(bot, config, message) {
    var game = message.content.split(' ').splice(1).join(' ').trim()

    if (game === '') {
        utils.sendMessage(message.channel, 'Please enter a valid game name')
        return
    }
    if (!(message.guild.id in config.games)) {
        config.games[message.guild.id] = []
    }
    var index = config.games[message.guild.id].indexOf(game)
    if (index == -1) {
    logger.log('info', `Adding game: ${game}`)
        config.games[message.guild.id].push(game)
        utils.saveJSON(config, 'config.json')
    }
}
