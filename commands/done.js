exports.run = done
exports.removeGroup = removeGroup
exports.desc = "Tell the bot you're finished playing"
exports.command = []

var utils = require('../utils.js')

/**
* Tells the bot your group is no longer playing.
* @param {Discord.Client} bot - The bot client instance
* @param {Object} config - JSON representation of the bot's current configuration
* @param {Discord.Message} message - The message that invoked this command
*/
function done(bot, config, message) {
    Object.keys(config.groups[message.guild.id]).some(function(groupID) {
        var group = config.groups[message.guild.id][groupID]
        if (message.channel.id === group.text) {
            removeGroup(group, config, bot)
        }

        return message.channel.id === group.text
    })
}

/**
* Removes a group from config.json.
* Also deletes roles and channels associated with the group.
* @param {Object} group - The group to remove
* @param {Object} config - The bot's current configuration
* @param {Discord.Client} bot - The current instance of the bot
*/
function removeGroup(group, config, bot) {
    var guild = bot.guilds.get(group.guild)
    guild.channels.get(group.text).delete()
    guild.channels.get(group.voice).delete()
    guild.roles.get(group.role).delete()
    delete config.groups[group.guild][group.users[0]]
    utils.saveJSON(config, 'config.json')
}
