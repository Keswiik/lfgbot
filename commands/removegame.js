exports.run = removegame
exports.desc = "Removes a game from the list of a guild's lfg games"
exports.command = ["removegame <game>"]

var utils = require('../utils.js')

/**
* Removes a game from the current server's list of games
* @param {Discord.Client} bot - The bot client instance
* @param {Object} config - JSON representation of the bot's current configuration
* @param {Discord.Message} message - The message that invoked this command
*/
function removegame(bot, config, message) {
    var game = message.content.split(' ').splice(1).join(' ').trim()

    if (game === '') {
        utils.sendMessage(message.channel, 'No game supplied.')
        return
    }

    var index = config.games[message.guild.id].indexOf(game)
    if (index > -1) {
        config.games[message.guild.id].splice(index, 1)
        utils.saveJSON(config, 'config.json')
    } else {
        utils.sendMessage(message.channel, `${message.author}, ${game} is not registered on this server`)
    }
}
