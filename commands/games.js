exports.run = games
exports.desc = "Show games playes can queue for in their server"
exports.command = []
var utils = require('../utils.js')

/**
* Sends a message showing what games are available on the current server.
* @param {Discord.Client} bot - The bot client instance
* @param {Object} config - JSON representation of the bot's current configuration
* @param {Discord.Message} message - The message that invoked this command
*/
async function games(bot, config, message) {
    var games = config.games[message.guild.id]
    var gameString
    if (games.length === 0) {
        gameString = 'Your server has not added support for any games'
    } else {
    gameString = 'Games supported by your server: '
    games.forEach(function(game) {
        gameString += `\n\t${game}`
    })
    }
    utils.sendMessage(message.channel, gameString)
}
