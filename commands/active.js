exports.run = active
exports.desc = "Tell the bot your group is still active"
exports.command = []

var utils = require('../utils.js')

/**
* Tells the bot your group is still active.
* Group will be removed next update cycle if this is not called.
* @param {Discord.Client} bot - The bot client instance
* @param {Object} config - JSON representation of the bot's current configuration
* @param {Discord.Message} message - The message that invoked this command
*/
function active(bot, config, message) {
    Object.keys(config.groups[message.guild.id]).some(function(groupID) {
        var group = config.groups[message.guild.id][groupID]
        if (message.channel.id === group.text) {
            group.preserve = true
            utils.saveJSON(config, 'config.json')
            utils.sendMessage(message.channel, 'Thanks for the heads up!')
        }
    })
}
