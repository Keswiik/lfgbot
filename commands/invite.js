exports.run = invite
exports.desc = "Get an invite link for this bot"
exports.command = [] 

var utils = require('../utils.js')

/**
* Sends a message containing an invite link for the bot
* @param {Discord.Client} bot - The bot client instance
* @param {Object} config - JSON representation of the bot's current configuration
* @param {Discord.Message} message - The message that invoked this command
*/
async function invite(bot, config, message) {
    utils.sendMessage(message.channel, await bot.generateInvite(['READ_MESSAGES', 'SEND_MESSAGES',
        'MANAGE_CHANNELS', 'MANAGE_ROLES', 'MANAGE_MESSAGES']))
}
