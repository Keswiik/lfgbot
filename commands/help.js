exports.run = help
exports.desc = "Get a list of commands"
exports.command = []

var utils = require('../utils.js')
var sprintf = require('sprintf-js').sprintf


/**
* Sends a message showing all known bot commands.
* @param {Discord.Client} bot - The bot client instance
* @param {Object} config - JSON representation of the bot's current configuration
* @param {Discord.Message} message - The message that invoked this command
*/
async function help(bot, config, message) {
    //returns a list of all memes the bot knows
      var list = getFormattedCommandList(config)
      var currentMessage = ''
      var line = 0
      while (line < list.length) {
        while (line < list.length && currentMessage.length + list[line].length < 1994) {
          currentMessage += list[line]
          line++
        }
        utils.sendMessage(message.channel, '```' + currentMessage + '```')
        currentMessage = ''
      }
}

/**
* Creates an array of strings that represent each line of the help message
* @param {Object} config - The bot's current configuration
*/
function getFormattedCommandList() {
    var commands = utils.getCommands()
    var formattedMessage = ['\nCommands\n']
    Object.keys(commands).sort().forEach(function (commandName) {
        var data = commands[commandName]
        formattedMessage.push(sprintf('\t%-10s: %s\n', commandName, data.desc))
        data.command.forEach(function (usage) {
            formattedMessage.push(`\t${' '.repeat(17)}${usage}\n`)
        })
    })

    return formattedMessage
}
