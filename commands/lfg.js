exports.run = lfg
exports.removeUserFromLobby = removeUserFromLobby
exports.retrackLobbies = retrackLobbies
exports.desc = "Registers a user to search for a group"
exports.command = [
    "lfg <number_of_people> \"<game>\"",
    "lfg <number_of_people> (uses server's first game)"
]

var utils = require('../utils.js')
var logger = require('../Logger.js')
var Discord = require('discord.js')
var collectors = []
var config, bot, message

/**
* Permissions for the @everyone role, applied to group text and voice channels.
* Only people in the group should be able to send messages and join chat.
*/
const permOverwritesGeneral = {
    READ_MESSAGES: false,
    SEND_MESSAGES: false,
    CONNECT: false,
}

/**
* Permissions for group roules, applied to group text and voice channels.
* Gives them the permissions necessary to connect to chat and/or send messages
*/
const permOverwritesGroup = {
    READ_MESSAGES: true,
    SEND_MESSAGES: true,
    SEND_TTS_MESSAGES: false,
    READ_MESSAGE_HISTORY: true,
    ATTACH_FILES: true,
    CONNECT: true,
    SPEAK: true,
    USE_VAD: true,
    EMBED_LINKS: true,
    ADD_REACTIONS: true,
}

/**
* Permissions for the bot, applied to group text and voice channels.
* I needs to be able to see how many people are in voice chat, OR
* how long ago the last message was sent.
*/
const permOverwritesBot = {
    MANAGE_ROLES: true,
    MANAGE_CHANNELS: true,
    CONNECT: true,
    READ_MESSAGES: true,
    SEND_MESSAGES: true
}

/**
* Tells the bot you wish to look for a group to play games with
* @param {Discord.Client} bot - The bot client instance
* @param {Object} config - JSON representation of the bot's current configuration
* @param {Discord.Message} message - The message that invoked this command
*/
async function lfg(_bot, _config, _message) {
    bot = _bot
    config = _config
    message = _message
    //parsing arguments
    var data = message.content.slice(message.content.indexOf(' ') + 1)
    var groupSize = parseInt(data.slice(0, data.indexOf(' ') + 1))
    data = data.slice(data.indexOf(' ') + 1)
    var game //if not supplied in arguments, use server's first game
    if (data.indexOf('\"') > -1) {
        game = data.slice(data.indexOf('\"') + 1, data.lastIndexOf('\"'))
    } else {
        game = config.games[message.guild.id][0]
    }

    //some basic validation (game / group number)
    if (config.games[message.guild.id].indexOf(game) === -1) {
        utils.sendMessage(message.channel,
            `${message.author}, ${game} is not registered on this server.`)
    }
    if (groupSize <= 1) {
        utils.sendMessage(message.channel,
            `${message.author}, groups must contain at least two people`)
    }

    var searchInfo = {
        groupSize: groupSize,
        game: game
    }

    //try to find an open lobby that matches user prefs
    var foundLobby = false
    Object.keys(config.lobbies[message.guild.id]).some(function(lobbyKey) {
        var lobby = config.lobbies[message.guild.id][lobbyKey]
        if (lobby.info.groupSize === searchInfo.groupSize &&
            lobby.info.game === searchInfo.game) { //matching lobby found
                utils.sendMessage(message.channel, 'Found a matching lobby!' +
                    ` Adding you to ${lobby.leader}'s group!`)
                //Add to lobby and update embed
                lobby.members.push(message.author.id)
                updateLobbyEmbed(lobby)
                foundLobby = true
                return true
        }
    })

    //must create a new lobby if no matching one is found
    if (!foundLobby) {
        createLobby(message.guild, message.author, searchInfo)
    }
}
/**
* Creates a new lobby for players looking for a group to play with.
* Players can join the lobby via reacts (or using !lfg with similar options)
* @param {Discord.Guild} guild - The guild the lobby is in
* @param {Discord.User} user - The user the new lobby will be created with
* @param {Object} prefs - The preferences for the new lobby (size / game)
*/
async function createLobby(guild, user, prefs) {
    lobby = {
        guild: guild.id,
        leader: user.id,
        info: prefs,
        members: [user.id]
    }
    var memberString = `${user.username}#${user.discriminator}`
    var lobbyEmbed = new Discord.RichEmbed()
        .setAuthor(prefs.game)
        .setTitle(`Lobby Leader: ${user.username}#${user.discriminator}`)
        .setColor('#00FF00')
        .addField('Player Status', `${lobby.members.length}/${lobby.info.groupSize}`, true)
        .addField('Players:', memberString, true)

    var lobbyChannel = guild.channels.get(config.lfg_lobby_channels[guild.id])
    var lobbyMessage = await lobbyChannel.send({embed: lobbyEmbed})
    await lobbyMessage.react('➕')
    await lobbyMessage.react('➖')
    lobby.message = lobbyMessage.id
    //use ReactionCollector to be notified when people reacts to lobby post
    collectors.push(new Discord.ReactionCollector(lobbyMessage, collectFilter))
    config.lobbies[guild.id][lobbyMessage.id] = lobby
    utils.saveJSON(config, 'config.json')
}

/**
* Creates a new group within a guild.
* Uses a lobby's information to create roles / channels for users in the group
* @param {Object} lobby - the lobby to make a group from
*/
function makeGroup(lobby) {
    logger.log('info', `Users in group: ${lobby.members}`)
    group = {
        users: lobby.members,
        guild: lobby.guild,
        game: lobby.info.game,
        name: `${utils.replaceAll(lobby.info.game, ' ', '')}-${lobby.leader}`,
        preserve: true
    }

    //should only be true is something goes horribly wrong...
    if (!(group.guild in config.groups)) {
        config.groups[group.guild] = {}
    }

    config.groups[group.guild][group.users[0]] = group
    utils.saveJSON(config, 'config.json')
    groupSetup(group)
}

/**
* Creates group roles, channels, and applies necessary permissions
* @param {Object} group - The group to perform setup for
*/
async function groupSetup(group) {
    var guild = bot.guilds.get(group.guild)
    var botRole = guild.roles.find('name', bot.user.username) //get integration role

    try {
        logger.log('info', `Setting up roles and channels for group ${group.name}`)
        const role = await guild.createRole( { //group-specific role creation
            name: group.name,
            color: 'YELLOW',
            mentionable: true
        })

        logger.log('info', `Created role ${role.name} [${role}]`)

        config.groups[group.guild][group.users[0]]['role'] = role.id

        //add group role to all group members
        group.users.forEach(async function(user) {
                const member = await guild.members.get(user).addRole(role)
                logger.log('info', `Added role ${role.name} to user ${member.name}`)
        })

        //create text channel and apply permissions
        var textChannel = await guild.createChannel(group.name, 'text')
        logger.log('info', `Created text channel ${textChannel}`)
        await textChannel.overwritePermissions(role, permOverwritesGroup)
        await textChannel.overwritePermissions(guild.defaultRole, permOverwritesGeneral)
        await textChannel.overwritePermissions(botRole, permOverwritesBot)
        config.groups[group.guild][group.users[0]].text = textChannel.id

        //create voice channel and apply permissions
        var voiceChannel = await guild.createChannel(group.name, 'voice')
        logger.log('info', `Created voice channel ${voiceChannel}`)
        await voiceChannel.overwritePermissions(role, permOverwritesGroup)
        await voiceChannel.overwritePermissions(guild.defaultRole, permOverwritesGeneral)
        await voiceChannel.overwritePermissions(botRole, permOverwritesBot)
        config.groups[group.guild][group.users[0]].voice = voiceChannel.id

        //format confirmation message and send it to all group membersuui
        var groupConfirmationString = `You've been added to group **${group.name}** in **${guild.name}**`
        groupConfirmationString += '\n\nThe members of this group are'
        group.users.forEach(function(userID) {
            const member = guild.members.get(userID)
            groupConfirmationString += `\n${member.user.username}#${member.user.discriminator}`
        })
        groupConfirmationString += `\n\nYou can talk to your teammates using the following channels`
        groupConfirmationString += `\nVoice: ${await voiceChannel.createInvite()}`
        groupConfirmationString += `\nText: ${await textChannel.createInvite()}`
        group.users.forEach(function(userID) {
            utils.sendMessage(guild.members.get(userID), groupConfirmationString)
        })

        utils.saveJSON(config, 'config.json')
    } catch (err) {
        logger.log('error', err.message)
        logger.log('error', err.stack)
    }
}

/**
* A filter for adding/removing users from lobbies when reactions are added to
* lobby messages
* @param {Discord.MessageReaction} messageReaction - the reaction that was added
* @param {Discord.Collector} collector - The collector that collected this reaction
*/
async function collectFilter(messageReaction, collector) {
    if (messageReaction.users.lastKey() === bot.user.id) {
        return
    }
    logger.log('info', `Received reaction from User ${messageReaction.users.lastKey()} with emoji ${messageReaction.emoji.name}`)

    var lobby = config.lobbies[messageReaction.message.guild.id][messageReaction.message.id]
    var userInLobby = lobby.members.indexOf(messageReaction.users.lastKey()) > -1

    // If plus react, trying to join lobby
    if (messageReaction.emoji.name === '➕') {
        if (!userInLobby) { // can't join lobby that you're already in
            addUserToLobby(messageReaction)
        }
    // Otherwise trying to leave lobby
    } else if (messageReaction.emoji.name === '➖') {
        if (userInLobby) {
            removeUserFromLobby(messageReaction.users.last(), messageReaction)
        } else { // can't leave lobby that you're not in
            await messageReaction.remove(messageReaction.users.lastKey())
            logger.log('info', `User ${messageReaction.users.lastKey()} is not in lobby, removing reaction`)
        }
    } else {
        messageReaction.remove(messageReaction.users.lastKey()) // remove all reactions that are not + or -
    }
}

/**
* Adds a user to an open lobby
* TODO: Make this not ugly AF to look at.
* @param {Discord.MessageReaction} messageReaction - The reaction to use
*/
async function addUserToLobby(messageReaction) {
    config.lobbies[messageReaction.message.guild.id][messageReaction.message.id].members.push(messageReaction.users.lastKey())
    logger.log('info', `User ${messageReaction.users.lastKey()} added to lobby ${messageReaction.message.id}`)
    var minusReacts = messageReaction.message.reactions.find(val => val.emoji.name === '➖')
    if (minusReacts.users.has(messageReaction.users.lastKey())) {
        minusReacts.remove(messageReaction.users.lastKey())
        logger.log('info', `User ${messageReaction.users.lastKey()} has been removed from the minus reactions`)
    }

    if (config.lobbies[messageReaction.message.guild.id][messageReaction.message.id].members.length ==
        config.lobbies[messageReaction.message.guild.id][messageReaction.message.id].info.groupSize) {
            makeGroup(lobby)
    } else {
        updateLobbyEmbed(config.lobbies[messageReaction.message.guild.id][messageReaction.message.id])
        utils.saveJSON(config, 'config.json')
    }
}

/**
* Removes a user from a lobby
* @param {Discord.User} user - The user to remove
* @param {Object} data - Either a Guild or MessageReaction. Required to find the lobby the user is in.
*/
async function removeUserFromLobby(user, data) {
    logger.log('info', `${Object.getPrototypeOf(data).toString.call(data)}`)
    var lobbyID, guildID, message
    if (data instanceof Discord.Guild) { //determining type of data to find needed values
        guildID = data.id
        config.lobbies[data.id].some(function(lobby) {
            if (lobby.members.indexOf(user.id) > 0) {
                lobbyID = lobby.message
                return true
            }
            return false
        })
        message = await guild.channels.get(config.lfg_lobby_channels[guild.id]).fetchMessage(lobbyID)
    } else if (data instanceof Discord.MessageReaction) {
        lobbyID = data.message.id
        guildID = data.message.guild.id
        message = data.message
    } else { // must be either Guild or MessageReaction, anything else not allowed
        logger.log('error', `Unsupported data type ${typeof data}`)
        return
    }

    var lobby = config.lobbies[guildID][lobbyID]
    var userIndex = lobby.members.indexOf(user.id)
    config.lobbies[guildID][lobbyID].members.splice(userIndex, 1) //removing user
    if (config.lobbies[guildID][lobbyID].members.length === 0) { //remove lobby if it is empty
        deleteLobby(config.lobbies[guildID][lobbyID])
    } else { //if the user is leaving, remove their + react (if it is there)
        var plusReacts = message.reactions.find(val => val.emoji.name === '➕')
        if (plusReacts.users.has(user.id)) {
            plusReacts.remove(user.id)
            logger.log('info', `User ${user} has been removed from the plus reactions`)
        }
        updateLobbyEmbed(config.lobbies[guildID][lobbyID])
        utils.saveJSON(config, 'config.json')
    }
}

/**
* Deletes a lobby from config.json
* @param {Object} lobby - The lobby to delete
*/
async function deleteLobby(lobby) {
    var guild = bot.guilds.get(lobby.guild)
    var message = await guild.channels.get(config.lfg_lobby_channels[guild.id]).fetchMessage(lobby.message)
    await message.delete()
    delete config.lobbies[guild.id][message.id]
    utils.saveJSON(config, 'config.json')
}

async function updateLobbyEmbed(lobby) {
    var guild = bot.guilds.get(lobby.guild)
    var lobbyLeader = guild.members.get(lobby.leader).user
    var message = await guild.channels.get(config.lfg_lobby_channels[guild.id]).fetchMessage(lobby.message)
    var memberString = ''
    lobby.members.forEach(function (member, index, arr) {
        var user = guild.members.get(member).user
        memberString += `${user.username}#${user.discriminator}${index === arr.length -1 ? '' : '\n'}`
    })
    var embed = new Discord.RichEmbed()
        .setAuthor(lobby.info.game)
        .setTitle(`Lobby Leader: ${lobbyLeader.username}#${lobbyLeader.discriminator}`)
        .setColor('#00FF00')
        .addField('Player Status', `${lobby.members.length}/${lobby.info.groupSize}`, true)
        .addField('Players:', memberString, true)
    await message.edit({embed})
    logger.log('info', `Updated embed for lobby ${lobby.message}`)
}

function retrackLobbies(_bot, _config) {
    bot = _bot
    config = _config

    Object.keys(config.lobbies).forEach(function (guildID) {
        Object.keys(config.lobbies[guildID]).forEach(async function(lobbyID) {
            var lobby = config.lobbies[guildID][lobbyID]
            var guild = bot.guilds.get(guildID)
            var trackMessage = await guild.channels.get(config.lfg_lobby_channels[guild.id]).fetchMessage(lobby.message)
            collectors.push(new Discord.ReactionCollector(trackMessage, collectFilter))
        })
    })
}
