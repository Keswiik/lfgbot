exports.run = setprefix
exports.desc = "Allows server owner to set bot prefix"
exports.command = ["setprefix <one_character_prefix>"]

var utils = require ('../utils.js')

/**
* Sets the bot's command prefix for the current server.
* @param {Discord.Client} bot - The bot client instance
* @param {Object} config - JSON representation of the bot's current configuration
* @param {Discord.Message} message - The message that invoked this command
*/
function setprefix(bot, config, message) {
    var prefix = message.content.slice(message.content.indexOf(' ') + 1)
    if (prefix.length != 1 || prefix == ' ' || prefix === '\"') { return }
    else {
        config.prefixes[message.guild.id] = prefix
        utils.saveJSON(config, 'config.json')
    }
}
