module.exports = {
    log: log
}

var levelColors = {
    info: 'fgGreen',
    error: 'fgRed'
}

var consoleColors = {
    reset: "\x1b[0m",
    bright: "\x1b[1m",
    dim: "\x1b[2m",
    underscore: "\x1b[4m",
    blink: "\x1b[5m",
    reverse: "\x1b[7m",
    hidden: "\x1b[8m",
    fgBlack: "\x1b[30m",
    fgRed: "\x1b[31m",
    fgGreen: "\x1b[32m",
    fgYellow: "\x1b[33m",
    fgBlue: "\x1b[34m",
    fgMagenta: "\x1b[35m",
    fgCyan: "\x1b[36m",
    fgWhite: "\x1b[37m",
    bgBlack: "\x1b[40m",
    bgRed: "\x1b[41m",
    bgGreen: "\x1b[42m",
    bgYellow: "\x1b[43m",
    bgBlue: "\x1b[44m",
    bgMagenta: "\x1b[45m",
    bgCyan: "\x1b[46m",
    bgWhite: "\x1b[47m"
}

var fs = require('fs')
var os = require('os')

require('dotenv').config()
if (!fs.existsSync(process.env.LOGFILE)) {
    fs.closeSync(fs.openSync(process.env.LOGFILE, 'w'))
}

async function log(level, content) {
    var date = new Date()
    var day = formatNumber(date.getUTCDay())
    var month = formatNumber(date.getUTCMonth() + 1)
    var year = date.getUTCFullYear()
    var hour = formatNumber(date.getUTCHours())
    var minute = formatNumber(date.getUTCMinutes())
    var second = formatNumber(date.getUTCSeconds())
    var dateString = `${day}.${month}.${year}-${hour}:${minute}:${second}`
    var indentLength = 5 + dateString.length + level.length
    var logstr = ''
    if (content.indexOf('\n') > -1) {
        var data = content.slice(0, content.indexOf('\n'))
        console.log(`[${dateString}] ${formatLevel(level)}: ${data}`)
        logstr += `[${dateString}] ${level.toUpperCase()}: ${data}`
        var dataArr = content.slice(content.indexOf('\n'), content.length -1).split('\n')
        var padding = " ".repeat(indentLength)
        dataArr.forEach(function(info) {
            console.log(`${padding}${info}`)
            logstr += `${padding}${info}${os.EOL}`
        })
    } else {
        console.log(`[${dateString}] ${formatLevel(level)}: ${content}`)
        logstr += `[${dateString}] ${level.toUpperCase()}: ${content}`
    }
    fs.appendFileSync(process.env.LOGFILE, logstr + os.EOL)
}

function formatLevel(level) {
    return `${consoleColors[levelColors[level]]}${level.toUpperCase()}${consoleColors.reset}`
}

function formatNumber(number) {
    return (number < 10) ? '0' + number : number
}
