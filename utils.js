module.exports = {
    replaceAll: replaceAll,
    saveJSON: saveJSON,
    minuteDiff: minuteDiff,
    sendMessage: sendMessage,
    getCommands: getCommands
}

var commands

var fs = require('fs')
var logger = require('./Logger.js')

/**
* Replaces all instances of a string within another string
* @param {String} str - The string to replace values in
* @param {String} find - The string to search for and replace
* @param {String} replace - The string to replace 'find' with
*/
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace)
}

/**
* Saves a JSON file.
* @param {Object} json - The JSON object to save
* @param {String} file_dir - The location of the file to save data to
*/
function saveJSON(json, file_dir) {
  fs.writeFile(file_dir, JSON.stringify(json, null, 4), "utf8", function() {
    logger.log("info", `File ${file_dir} was updated`)
  })
}

/**
* Get the number of minutes between two dates
* Can probably be moved to neatbot.js
* @param {Date} date1 - The first date
* @param {Date} date2 - The second date
*/
function minuteDiff(date1, date2) {
    return Math.abs(date1 - date2) / (1000 * 60)
}

/**
* Sends a message to a channel, logging what message was sent
* @param {Discord.TextChannel} channel - The channel to send messages to
* @param {String} message - The message to send
*/
async function sendMessage(channel, message) {
    try {
        var sentMessage = await channel.send(message)
        logger.log('info', `Sent message:\n${message}`)
    } catch (err) {
        logger.log('error', err.message)
        logger.log('error', err.stack)
    }
}

function getCommands() {
    if (!commands) {
        try {
            commands = {}
            var commandNames = fs.readdirSync('./commands/')
            commandNames.forEach(function (value, index, arr) {
                if (value.indexOf('.js') === -1) {
                    commandNames.splice(index, 1)
                } else {
                    var name = value.slice(0, value.indexOf('.'))
                    commands[name] = require(`./commands/${value}`)
                }
            })
        } catch (err){
            logger.log('error', err.message)
            logger.log('error', err.stack)
        }
    }
    return commands
}
