require('dotenv').config()

var Discord = require('discord.js')
var fs = require('fs')
var logger = require('./Logger.js')
var bot = new Discord.Client()
var config = JSON.parse(fs.readFileSync('config.json', 'utf8'))
var sprintf = require('sprintf-js').sprintf
var commands = {
    "lfg": lfg,
    "nlfg": nlfg,
    "addgame": addgame,
    "removegame": removegame,
    "setprefix": setprefix,
    "games": games,
    "active": active,
    "done": done,
    "invite": invite,
    "help": help
}

var collectors = []

const permOverwritesGeneral = {
    READ_MESSAGES: false,
    SEND_MESSAGES: false,
    CONNECT: false,
}

const permOverwritesGroup = {
    READ_MESSAGES: true,
    SEND_MESSAGES: true,
    SEND_TTS_MESSAGES: false,
    READ_MESSAGE_HISTORY: true,
    ATTACH_FILES: true,
    CONNECT: true,
    SPEAK: true,
    USE_VAD: true,
    EMBED_LINKS: true,
    ADD_REACTIONS: true,
}

const permOverwritesBot = {
    MANAGE_ROLES: true,
    MANAGE_CHANNELS: true,
    CONNECT: true,
    READ_MESSAGES: true,
    SEND_MESSAGES: true
}

bot.login(process.env.TOKEN)

bot.on('ready', async function() {
    logger.log('info', `Using Discord.js version ${Discord.version}`)
    logger.log('info', 'LFGBot is now online')
    const invite = await bot.generateInvite(['READ_MESSAGES', 'SEND_MESSAGES',
        'MANAGE_CHANNELS', 'MANAGE_ROLES', 'MANAGE_MESSAGES'])
    logger.log('info', `Use the following link to invite: ${invite}`)
    setInterval(checkGroupActivity, process.env.INACTIVITY_TIMEOUT * 60000)
})

bot.on('message', function(message) {
    var validChannel = false
    if (message.author.id === bot.user.id) {
        return
    }

    if (message.content.startsWith(config.prefixes[message.guild.id])) {
        var command = message.content.slice(1).split(' ')[0]
        if (command in config.commands) {
            logger.log('info', `Executing command ${command}`)
            commands[command](message)
        }
    }
})

bot.on('guildCreate', async function(guild) {
    logger.log('info', `Joined guild ${guild.name} (${guild.id})`)
    try {
        var channel = await guild.createChannel("looking-for-group", "text")
        logger.log('info', `Created channel ${channel}`)

        var lobbyChannel = await guild.createChannel("looking-for-group-lobbies", "text")
        logger.log('info', `Created channel ${lobbyChannel}`)

        var role = await guild.createRole({
            name: 'LFG',
            color: 'ORANGE',
            mentionable: true
        })
        logger.log('info', `Created role ${role}`)

        config.lfg_channels[guild.id] = channel.id
        config.lfg_roles[guild.id] = role.id
        config.lfg_lobby_channels[guild.id] = lobbyChannel.id
        config.lobbies[guild.id] = {}
    } catch (err) {
        logger.log('error', err.message)
        logger.log('error', err.stack)
    }

    config.prefixes[guild.id] = config.prefixes.default
    saveJSON(config, 'config.json')
})

process.on('unhandledRejection', err => {
    logger.log('error', err.message);
    logger.log('error', err.stack)
});

async function collectFilter(messageReaction, collector) {
    if (messageReaction.users.lastKey() === bot.user.id) {
        return
    }
    logger.log('info', `Received reaction from User ${messageReaction.users.lastKey()} with emoji ${messageReaction.emoji.name}`)

    var lobby = config.lobbies[messageReaction.message.guild.id][messageReaction.message.id]
    var userInLobby = lobby.members.indexOf(messageReaction.users.lastKey()) > -1

    if (messageReaction.emoji.name === '➕') {
        if (userInLobby) {
            await messageReaction.remove()
            logger.log('info', `User ${messageReaction.users.lastKey()} already in lobby, removing reaction`)
        } else {
            addUserToLobby(messageReaction)
        }
    } else if (messageReaction.emoji.name === '➖') {
        if (userInLobby) {
            removeUserFromLobby(messageReaction)
        } else {
            logger.log('info', `User ${messageReaction.users.lastKey()} is not in lobby, removing reaction`)
        }
    } else {
        messageReaction.remove()
    }
}

async function invite(message) {
    sendMessage(message.channel, await bot.generateInvite(['READ_MESSAGES', 'SEND_MESSAGES',
        'MANAGE_CHANNELS', 'MANAGE_ROLES', 'MANAGE_MESSAGES']))
}

async function removeUserFromLobby(messageReaction) {
    var userIndex = lobby.members.indexOf(messageReaction.users.lastKey())
    config.lobbies[messageReaction.message.guild.id][messageReaction.message.id].members.splice(userIndex, 1)
    if (config.lobbies[messageReaction.message.guild.id][messageReaction.message.id].members.length === 0) {
        deleteLobby(config.lobbies[messageReaction.message.guild.id][messageReaction.message.id])
    } else {
        var plusReacts = messageReaction.message.reactions.find(val => val.emoji.name === '➕')
        if (plusReacts.users.has(messageReaction.users.lastKey())) {
            plusReacts.remove(messageReaction.users.lastKey())
            logger.log('info', `User ${messageReaction.users.lastKey()} has been removed from the plus reactions`)
        }
        logger.log('info', `User ${messageReaction.users.lastKey()} has been removed from lobby ${messageReaction.message.id}`)
        updateLobbyEmbed(config.lobbies[messageReaction.message.guild.id][messageReaction.message.id])
        saveJSON(config, 'config.json')
    }
}

async function addUserToLobby(messageReaction) {
    config.lobbies[messageReaction.message.guild.id][messageReaction.message.id].members.push(messageReaction.users.lastKey())
    logger.log('info', `User ${messageReaction.users.lastKey()} added to lobby ${messageReaction.message.id}`)
    var minusReacts = messageReaction.message.reactions.find(val => val.emoji.name === '➖')
    if (minusReacts.users.has(messageReaction.users.lastKey())) {
        minusReacts.remove(messageReaction.users.lastKey())
        logger.log('info', `User ${messageReaction.users.lastKey()} has been removed from the minus reactions`)
    }

    if (config.lobbies[messageReaction.message.guild.id][messageReaction.message.id].members.length ==
        config.lobbies[messageReaction.message.guild.id][messageReaction.message.id].info.groupSize) {
            makeGroup(lobby)
    } else {
        updateLobbyEmbed(config.lobbies[messageReaction.message.guild.id][messageReaction.message.id])
        saveJSON(config, 'config.json')
    }
}

async function deleteLobby(lobby) {
    var guild = bot.guilds.get(lobby.guild)
    var message = await guild.channels.get(config.lfg_lobby_channels[guild.id]).fetchMessage(lobby.message)
    await message.delete()
    delete config.lobbies[guild.id][message.id]
    saveJSON(config, 'config.json')
}

async function lfg(message) {
    var data = message.content.slice(message.content.indexOf(' ') + 1)
    var groupSize = parseInt(data.slice(0, data.indexOf(' ') + 1))
    data = data.slice(data.indexOf(' ') + 1)
    var game = data.slice(data.indexOf('\"') + 1, data.lastIndexOf('\"'))

    var searchInfo = {
        groupSize: groupSize,
        game: game
    }
    var foundLobby = false
    logger.log('info', `${config.lobbies[message.guild.id]}`)
    Object.keys(config.lobbies[message.guild.id]).some(function(lobbyKey) {
        var lobby = config.lobbies[message.guild.id][lobbyKey]
        if (lobby.info.groupSize === searchInfo.groupSize &&
            lobby.info.game === searchInfo.game) {
                sendMessage(message.channel, 'Found a matching lobby!' +
                    ` Adding you to ${lobby.leader}'s group!`)
                updateLobbyEmbed(lobby)
                foundLobby = true
                return true
        }
    })

    if (!foundLobby) {
        createLobby(message.guild, message.author, searchInfo)
    }
}

async function createLobby(guild, user, prefs) {
    lobby = {
        guild: guild.id,
        leader: user.id,
        info: prefs,
        members: [user.id]
    }
    var lobbyEmbed = new Discord.RichEmbed()
        .setAuthor(prefs.game)
        .setTitle(`${user.username}#${user.discriminator}`)
        .setColor('#00FF00')
        .addField('Players', `${lobby.members.length}/${lobby.info.groupSize}`)

    var lobbyChannel = guild.channels.get(config.lfg_lobby_channels[guild.id])
    var lobbyMessage = await lobbyChannel.send({embed: lobbyEmbed})
    await lobbyMessage.react('➕')
    await lobbyMessage.react('➖')
    lobby.message = lobbyMessage.id
    collectors.push(new Discord.ReactionCollector(lobbyMessage, collectFilter))
    config.lobbies[guild.id][lobbyMessage.id] = lobby
    saveJSON(config, 'config.json')
}

async function nlfg(message) {

}

function makeGroup(lobby) {
    logger.log('info', `Users in group: ${lobby.members}`)
    group = {
        users: lobby.members,
        guild: lobby.guild,
        game: lobby.info.game,
        name: `${replaceAll(lobby.info.game, ' ', '')}-${lobby.leader}`,
        preserve: true
    }

    if (!(group.guild in config.groups)) {
        config.groups[group.guild] = {}
    }
    config.groups[group.guild][group.users[0]] = group
    groupSetup(group)
}

async function updateLobbyEmbed(lobby) {
    var guild = bot.guilds.get(lobby.guild)
    var message = await guild.channels.get(config.lfg_lobby_channels[guild.id]).fetchMessage(lobby.message)
    var lobbyLeader = await bot.fetchUser(lobby.leader)
    var embed = new Discord.RichEmbed()
        .setAuthor(lobby.info.game)
        .setTitle(`${lobbyLeader.username}#${lobbyLeader.discriminator}`)
        .setColor('#00FF00')
        .addField('Players', `${lobby.members.length}/${lobby.info.groupSize}`)
    await message.edit({embed})
    logger.log('info', `Updated embed for lobby ${lobby.message}`)
}

async function groupSetup(group) {
    var guild = bot.guilds.get(group.guild)

    var botRole = guild.roles.find('name', bot.user.username)

    logger.log('info', `Setting up roles and channels for group ${group.name}`)
    try {
        const role = await guild.createRole( {
            name: group.name,
            color: 'YELLOW',
            mentionable: true
        })

        logger.log('info', `Created role ${role.name} [${role}]`)

        config.groups[group.guild][group.users[0]]['role'] = role.id

        group.users.forEach(async function(user) {
                const member = await guild.members.get(user).addRole(role)
                logger.log('info', `Added role ${role.name} to user ${member.name}`)
        })

        var textChannel = await guild.createChannel(group.name, 'text')
        logger.log('info', `Created text channel ${textChannel}`)
        await textChannel.overwritePermissions(role, permOverwritesGroup)
        await textChannel.overwritePermissions(guild.defaultRole, permOverwritesGeneral)
        await textChannel.overwritePermissions(botRole, permOverwritesBot)
        config.groups[group.guild][group.users[0]].text = textChannel.id

        var voiceChannel = await guild.createChannel(group.name, 'voice')
        logger.log('info', `Created voice channel ${voiceChannel}`)
        await voiceChannel.overwritePermissions(role, permOverwritesGroup)
        await voiceChannel.overwritePermissions(guild.defaultRole, permOverwritesGeneral)
        await voiceChannel.overwritePermissions(botRole, permOverwritesBot)
        config.groups[group.guild][group.users[0]].voice = voiceChannel.id

        var groupConfirmationString = `You've been added to group **${group.name}** in **${guild.name}**`
        groupConfirmationString += '\n\nThe members of this group are'
        group.users.forEach(function(userID) {
            const member = guild.members.get(userID)
            groupConfirmationString += `\n${member.user.username}#${member.user.discriminator}`
        })
        groupConfirmationString += `\n\nYou can talk to your teammates using the following channels`
        groupConfirmationString += `\nVoice: ${await voiceChannel.createInvite()}`
        groupConfirmationString += `\nText: ${await textChannel.createInvite()}`
        group.users.forEach(function(userID) {
            sendMessage(guild.members.get(userID), groupConfirmationString)
        })

        saveJSON(config, 'config.json')
    } catch (err) {
        logger.log('error', err.message)
        logger.log('error', err.stack)
    }
}

function removegame(message) {
    var game = message.content.slice(message.content.indexOf('\"') + 1,
        message.content.lastIndexOf('\"'))

    var index = config.games[message.guild.id].indexOf(game)
    if (index > 1) {
        config.games[message.guild.id].splice(index, 1)
        saveJSON(config, 'config.json')
    }
}

function setprefix(message) {
    var prefix = message.content.slice(message.content.indexOf(' ') + 1)
    if (prefix.length != 1 || prefix == ' ' || prefix === '\"') { return }
    else {
        config.prefixes[message.guild.id] = prefix
        saveJSON(config, 'config.json')
    }
}

async function games(message) {
    var games = config.games[message.guild.id]
    var gameString = 'Games supported by your server: '
    games.forEach(function(game) {
        gameString += `\n\t${game}`
    })
    await sendMessage(message.channel, gameString)
}

async function sendMessage(channel, message) {
    try {
        var sentMessage = await channel.send(message)
        logger.log('info', `Sent message:\n${message}`)
    } catch (err) {
        logger.log('error', err.message)
        logger.log('error', err.stack)
    }
}

// function replaceAll(str, find, replace) {
//     return str.replace(new RegExp(find, 'g'), replace)
// }
//
// function saveJSON(json, file_dir) {
//   fs.writeFile(file_dir, JSON.stringify(json, null, 4), "utf8", function() {
//     logger.log("info", `File ${file_dir} was updated`)
//   })
// }

async function checkGroupActivity() {
    logger.log('info', 'Checking for inactive groups')
    var current = new Date()
    Object.keys(config.groups).forEach(function (guildID) {
        Object.keys(config.groups[guildID]).forEach(async function(groupID) {
            var group = config.groups[guildID][groupID]
            var guild = bot.guilds.get(guildID)
            if (group.preserve) {
                var textChannel = guild.channels.get(group.text)
                var voiceChannel = guild.channels.get(group.voice)
                var lastMessage = null
                if (textChannel.lastMessageID != null) {
                    try {
                        lastMessage = await textChannel.fetchMessage(textChannel.lastMessageID)
                    } catch (err) {
                        logger.log('error', err.message)
                        logger.log('error', err.stack)
                    }
                }
                var textInactive = (lastMessage != null) ?
                    minuteDiff(current, lastMessage.createdAt) >= process.env.INACTIVITY_TIMEOUT :
                    true
                if (textInactive && voiceChannel.members.size == 0) {
                    group.preserve = false
                    var groupPreservationString = 'It seems your group is inactive!'
                    groupPreservationString += '\n\nIf you are still playing together, please reply '
                    groupPreservationString += `within ${process.env.INACTIVITY_TIMEOUT} minutes `
                    groupPreservationString += 'using \`\`\`!active\`\`\`'
                    sendMessage(textChannel, groupPreservationString)
                    saveJSON(config, 'config.json')
                }
            } else {
                removeGroup(group)
            }
        })
    })
}

function removeGroup(group) {
    var guild = bot.guilds.get(group.guild)
    guild.channels.get(group.text).delete()
    guild.channels.get(group.voice).delete()
    guild.roles.get(group.role).delete()
    delete config.groups[group.guild][group.users[0]]
    saveJSON(config, 'config.json')
}

function active(message) {
    Object.keys(config.groups[message.guild.id]).some(function(groupID) {
        var group = config.groups[message.guild.id][groupID]
        if (message.channel.id === group.text) {
            group.preserve = true
            saveJSON(config, 'config.json')
            sendMessage(message.channel, 'Thanks for the heads up!')
        }
    })
}

function done(message) {
    Object.keys(config.groups[message.guild.id]).some(function(groupID) {
        var group = config.groups[message.guild.id][groupID]
        if (message.channel.id === group.text) {
            removeGroup(group)
        }
    })
}

async function help(message) {
    //returns a list of all memes the bot knows
      var list = getFormattedCommandList()
      var currentMessage = ''
      var line = 0
      while (line < list.length) {
        while (line < list.length && currentMessage.length + list[line].length < 1994) {
          currentMessage += list[line]
          line++
        }
        await sendMessage(message.channel, '```' + currentMessage + '```')
        currentMessage = ''
      }
}

function getFormattedCommandList() {
    var commands = config.commands
    var formattedMessage = ['\nCommands\n']
    Object.keys(commands).sort().forEach(function (commandName) {
        var data = config.commands[commandName]
        formattedMessage.push(sprintf('\t%-10s: %s\n', commandName, data.desc))
        data.command.forEach(function (usage) {
            formattedMessage.push(`\t${' '.repeat(17)}${usage}\n`)
        })
    })

    return formattedMessage
}
