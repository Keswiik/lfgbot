/**
* The Discord LFGBot.
* Author: Keswiik
* TODO: Remove try-catch statements from within commands and surround *.run() with try-catch instead.
* TODO: Fix up the LFG command to not be ugly.
* TODO: Finish testing this thing. Add more feedback to servers.
* TODO: Tag users of new groups within their private text channel, including a link to their voice chat.
*/

require('dotenv').config()
var Discord = require('discord.js')
var logger = require('./Logger.js')
var utils = require('./utils.js')
var fs = require('fs')
var bot = new Discord.Client()
var config = JSON.parse(fs.readFileSync('config.json', 'utf8'))
var commands = utils.getCommands()

bot.login(process.env.TOKEN)

bot.on('ready', async function() {
    commands.lfg.retrackLobbies(bot, config)
    logger.log('info', `Using Discord.js version ${Discord.version}`)
    logger.log('info', 'LFGBot is now online')
    const invite = await bot.generateInvite(['READ_MESSAGES', 'SEND_MESSAGES',
        'MANAGE_CHANNELS', 'MANAGE_ROLES', 'MANAGE_MESSAGES'])
    logger.log('info', `Use the following link to invite: ${invite}`)
    //setInterval(checkGroupActivity, process.env.INACTIVITY_TIMEOUT * 60000)
})

bot.on('message', function(message) {
    var validChannel = false
    if (message.author.id === bot.user.id) {
        return
    }

    if (message.isMentioned(bot.user) && message.content.indexOf('help') > 0) {
        commands.help.run(bot, config, message)
    }

    if (message.content.startsWith(config.prefixes[message.guild.id])) {
        var command = message.content.slice(1).split(' ')[0]
        if (command === 'reload') {
            var toReload = message.content.split(' ')[1]
            if (toReload in commands) {
                delete require.cache[require.resolve(`./commands/${toReload}.js`)]
                commands[toReload] = require(`./commands/${toReload}.js`)
                logger.log('info', `Reloaded command ${toReload}`)
            }
        } else if (command in commands) {
            commands[command].run(bot, config, message)
        }
    }
})

bot.on('guildCreate', async function(guild) {
    logger.log('info', `Joined guild ${guild.name} (${guild.id})`)
    try {
        var channel = await guild.createChannel("looking-for-group", "text")
        logger.log('info', `Created channel ${channel}`)

        var lobbyChannel = await guild.createChannel("looking-for-group-lobbies", "text")
        logger.log('info', `Created channel ${lobbyChannel}`)

        var role = await guild.createRole({
            name: 'LFG',
            color: 'ORANGE',
            mentionable: true
        })
        logger.log('info', `Created role ${role}`)

        config.lfg_channels[guild.id] = channel.id
        config.lfg_roles[guild.id] = role.id
        config.lfg_lobby_channels[guild.id] = lobbyChannel.id
        config.lobbies[guild.id] = {}
    } catch (err) {
        logger.log('error', err.message)
        logger.log('error', err.stack)
    }

    config.prefixes[guild.id] = config.prefixes.default
    utils.saveJSON(config, 'config.json')
})

process.on('unhandledRejection', err => {
    logger.log('error', err.message);
    logger.log('error', err.stack)
});



async function checkGroupActivity() {
    logger.log('info', 'Checking for inactive groups')
    var current = new Date()
    Object.keys(config.groups).forEach(function (guildID) {
        Object.keys(config.groups[guildID]).forEach(async function(groupID) {
            var group = config.groups[guildID][groupID]
            var guild = bot.guilds.get(guildID)
            if (group.preserve) {
                var textChannel = guild.channels.get(group.text)
                var voiceChannel = guild.channels.get(group.voice)
                var lastMessage = null
                if (textChannel.lastMessageID != null) {
                    try {
                        lastMessage = await textChannel.fetchMessage(textChannel.lastMessageID)
                    } catch (err) {
                        logger.log('error', err.message)
                        logger.log('error', err.stack)
                    }
                }
                var textInactive = (lastMessage != null) ?
                    utils.minuteDiff(current, lastMessage.createdAt) >= process.env.INACTIVITY_TIMEOUT :
                    true
                if (textInactive && voiceChannel.members.size == 0) {
                    group.preserve = false
                    var groupPreservationString = 'It seems your group is inactive!'
                    groupPreservationString += '\n\nIf you are still playing together, please reply '
                    groupPreservationString += `within ${process.env.INACTIVITY_TIMEOUT} minutes `
                    groupPreservationString += 'using \`\`\`!active\`\`\`'
                    utils.sendMessage(textChannel, groupPreservationString)
                    utils.saveJSON(config, 'config.json')
                }
            } else {
                commands.done.removeGroup(group, config, bot)
            }
        })
    })
}
