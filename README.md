# Keswiik's LFGBot

What is it?
------
A bot to help users of a Discord server find others to play with.

What setup is required?
------
So far, none! Upon joining your server, it will automatically set up channels for itself to use.  
Specifically, it will set up:
```
Roles:
    LFGBot (integration role)
    LFG
Channels:
    looking-for-group
    looking-for-group-lobbies
```

What permissions does LFGBot require?
------
LFGBot requires the following permissions to function:  
- `MANAGE_CHANNELS` to create channels for groups playing together  
- `MANAGE_ROLES` to manage group roles  
- `MANAGE_MESSAGES` to remove reactions from lobby messages  
- `READ_MESSAGES` because it can't see commands without this..  
- `SEND_MESSAGES` cannot respond to users / post lobbies without this   

Which \*bot.js file is the real one?
------
That would be neatbot.js.  
An explanation for the different files:  
- newbot.js was the first working version of this bot. Used lots of .then() statements, which I hate  
- awaitasyncbot.js is newbot.js, but using the await-async structure of js to improve readability  
- neatbot.js is the next iteration of awaitasyncbot.js, modularizing commands within the `commands` directory.  
It is also the only version of this bot with "real" documentation

How does it work?
------
1. Users will use `!lfg <number_of_people> "game"` to specify that they wish to find a group.
2. The bot will look for similar open lobbies. If none are found, a new lobby will be created.
3. As other users invoke `!lfg`, they will be added to the lobby, or they can manually join by using reactions in `looking-for-group-lobbies`
4. Once a lobby is full, its message will be deleted and new text/voice channels will be created for its members so they can play together.
5. Periodically, the bot will check groups for activity. If none is found, it will mark them for deletion on the next update cycle. Members of the group can invoke `!active` to prevent their group from being disbanded.

What's next?
------
- Making the bot's feedback a bit more useful.
- Better formatting for lobby embeds.
- Being able to tag the bot to ask for help.
- Maybe a real database instead of a JSON file. (I'm lazy.)
- Admin privileges for specific commands (adding/removing games, setting prefix)
- Actually implementing the `!nlfg` command...
- Tidying up error handling.
- Other stuff that I'm sure I've forgotten but will remember once I start working on this again.
