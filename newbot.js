require("dotenv").config()

//Needed to access the Discord API
var Discord = require('discord.js')

var fs = require('fs')

var logger = require("./Logger.js")

var bot = new Discord.Client()
var config = JSON.parse(fs.readFileSync("config.json", "utf8"))
var lfgArr = {}

var commands = {
    "lfg": lfg,
    "nlfg": nlfg,
    "addgame": addgame,
    "removegame": removegame,
    "setprefix": setprefix,
    "games": games,
    "ch": ch
}
// bot.guilds.forEach(function(id,guild) {
//     console.log(id)
//     lfg[id] = {}
// })

bot.login(process.env.TOKEN)

bot.on('ready', function() {
    bot.guilds.some(function(guild) {
        lfgArr[guild.id] = {}
    })
    console.log("LFGBot is now online.")
    bot.generateInvite(['READ_MESSAGES', 'SEND_MESSAGES', 'MANAGE_CHANNELS', 'MANAGE_ROLES']).then(invite => {
        console.log(`Use the following link to invite: ${invite}`);
    });
})

bot.on('message', function(message) {
    if (message.isMentioned(bot.user)) {
        console.log("[INFO] LFGBot was mentioned in a message")
    }
    // bot.guilds.get(message.guild.id).createRole({
    //     'name': "TEST",
    //     'color': 'YELLOW',
    //     'mentionable': true
    // })
    //     .then(role => console.log(`Created role ${role}`))
    //     .catch(console.error)
    if (message.author.id === bot.user.id) {
        return
    }
    if (message.content.startsWith(config.prefixes[message.guild.id])) {
        var command = message.content.slice(1).split(" ")[0]
        console.log(`Executing command ${command}`)
        if (command in config.commands) {
            commands[command](message)
        }
    }
})

bot.on('guildCreate', function(guild) {
    guild.createChannel("looking-for-group", "text")
        .then(channel => logger.log('info', `Created channel ${channel}`))
        .catch(console.error)

    guild.createRole({
        name: 'LFG',
        color: 'ORANGE',
        mentionable: true
    })
        .then(role => logger.log('info', `Created role ${role}`))
        .catch(console.error)

    config.prefixes[guild.id] = config.prefixes.default
})

function ch(message) {
    var channelID = message.content.split(' ')[1]
    logger.log('info', `${bot.channels.get(channelID)}`)
}

function lfg(message) {
    if (message.author.id in lfgArr[message.guild.id]) {
        logger.log('info' `User ${message.author} already looking for group`)
        return
    } else {
        var data = message.content.slice(message.content.indexOf(" ") + 1)
        var groupSize = parseInt(data.slice(0, data.indexOf(" ") + 1))
        data = data.slice(data.indexOf(" ") + 1)
        var game = data.slice(data.indexOf("\"") + 1, data.lastIndexOf("\""))

        lfgArr[message.guild.id][message.author.id] = {
            groupSize: groupSize,
            game: game
        }

        var group = makeGroup(message.guild.id, message.author.id)
        console.log(group)
        if (group != null) {
            group.users.forEach(function (userID) {
                delete lfgArr[message.guild.id][userID]
            })

            config.groups[group.users[0]] = group
            groupSetup(group, message)

            saveJSON(config, 'config.json')
        }
    }
}

function makeGroup(guild, user) {
    logger.log('info', 'Trying to make group')
    var users = []
    var group = null
    var searchInfo = lfgArr[guild][user]
    Object.keys(lfgArr[guild]).some(function(lfgUser) {
        logger.log('info', `Current user: ${lfgUser}`)
        if (lfgArr[guild][lfgUser].game === searchInfo.game
            && lfgArr[guild][lfgUser].groupSize === searchInfo.groupSize) {
                logger.log('info', 'Adding user to group')
                users.push(lfgUser)
                return searchInfo.groupSize === users.length
            }
        })
    logger.log('info', `Users in group: ${users}`)
    if (users.length === lfgArr[guild][user].groupSize) {
        group = {
            users: users,
            guild: guild,
            game: lfgArr[guild][user].name,
            name: replaceAll(lfgArr[guild][user].game, " ", "") + "-" + user
        }
        logger.log('info', `Group ${group.name} has been created`)
    }

        return group
    }

function nlfg(message) {

}

function groupSetup(group, message) {
    var guild = message.guild

    logger.log('info', `Setting up roles and channels for group ${group.name}`)

    bot.guilds.get(group.guild).createRole({
        'name': group.name,
        'color': 'YELLOW',
        'mentionable': true
    })
    .then(function(role) {
        console.log(`Created role ${role}`)
        group.users.forEach(function(user) {
            guild.members.get(user).addRole(role)
                .then(guildMember => logger.log('info', `Added role ${role} to ${guildMember}`))
                .catch(console.error)
            })

        var permOverwritesGeneral = {
            READ_MESSAGES: false,
            SEND_MESSAGES: false,
            SEND_TTS_MESSAGES: false,
            ATTACH_FILES: false,
            READ_MESSAGE_HISTORY: false,
            MENTION_EVERYONE: false,
            CONNECT: false,
            SPEAK: false,
        }

        var permOverwritesGroup = {
            READ_MESSAGES: true,
            SEND_MESSAGES: true,
            SEND_TTS_MESSAGES: false,
            READ_MESSAGE_HISTORY: true,
            ATTACH_FILES: true,
            CONNECT: true,
            SPEAK: true,
            USE_VAD: true,
            EMBED_LINKS: true,
            ADD_REACTIONS: true,
        }

        guild.createChannel(group.name, "text")
            .then(function(channel) {
                logger.log('info', `Created text channel ${channel}`)
                channel.overwritePermissions(role, permOverwritesGroup)
                        .then(() => logger.log('info', "Group permissions successfully applied"))
                        .catch(console.error)
                channel.overwritePermissions(guild.defaultRole, permOverwritesGeneral)
                        .then(() => logger.log('info', 'General permissions successfully applied'))
            })
            .catch(console.error)

        guild.createChannel(group.name, "voice")
            .then(function(channel){
                console.log(`[INFO] Created voice channel ${channel}`)
                channel.overwritePermissions(role, permOverwritesGroup)
                        .then(() => logger.log('info', "Group permissions successfully applied"))
                        .catch(console.error)
                channel.overwritePermissions(guild.defaultRole, permOverwritesGeneral)
                        .then(() => logger.log('info', "Group permissions successfully applied"))
            })
            .catch(console.error)

        var groupConfirmationString = `You've been added to group ${group.name}: `
        groupConfirmationString += "\n\nThe members of this group are: "
        group.users.forEach(function(userID) {
            groupConfirmationString += `\n${guild.members.get(userID)}`
        })
        groupConfirmationString += "\n\nYou can talk to your teammates using"
            + ` the text/voice channels named ${group.name}`

        group.users.forEach(function(userID) {
            guild.members.get(userID).send(groupConfirmationString)
                .then(message => logger.log('info', `Sent message ${message}`))
                .catch(console.error)
        })
    })
    .catch(console.error)

    // guild.createRole({
    //     'name': group.name,
    //     'color': 'YELLOW',
    //     'mentionable': true
    // })
    //     .then(function(role) {
    //         roleID = role.id
    //         console.log(`Create role ${role}`)
    //     })
    //     .catch(console.error)
}

async function test(guild) {
    var channel = await guild.createChannel("test", "text")
}

function addgame(message) {
    var game = message.content.slice(message.content.indexOf("\"") + 1,
        message.content.lastIndexOf("\""))

    if (game === "" || game === " " || game === String(null)) {
        message.channel.send("Please enter a valid game name.")
    }

    if (message.guild.id in config.games) {
        config.games[message.guild.id].push(game)
    } else {
        config.games[message.guild.id] = [game]
    }

    saveJSON(config, 'config.json')
}

function removegame(message){
    var game = message.content.slice(message.content.indexOf("\"") + 1,
    message.content.lastIndexOf("\""))
    index = config.games[message.guild.id].indexOf(game)
    if (index > -1) {
        config.games[message.guild.id].splice(index, 1)
    }

    saveJSON(config, 'config.json')
}

function setprefix(message) {
    var prefix = message.content.slice(message.content.indexOf(" ") + 1)
    if (prefix.length != 1 || prefix === " " || prefix === "\"") {
        return
    } else {
        config.prefixes[message.guild.id] = prefix
    }
}

function games(message) {
    var games = config.games[message.guild.id];
    message.channel.send("Games supported by your server: " + games.toString())
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace)
}

function saveJSON(json, file_dir) {
  fs.writeFile(file_dir, JSON.stringify(json, null, 4), "utf8", function() {
    logger.log("info", `File ${file_dir} was updated`)
  })
}
